The shared code is in the Shared folder.

- common: The common code
- android: Some platform specific code for android, which will be included in the android project as an external dependencies.
- ios: Some platform specific code for iOS, it will be compiled as an iOS framework
