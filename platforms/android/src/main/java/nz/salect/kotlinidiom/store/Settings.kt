package nz.salect.kotlinidiom.store

import android.content.Context
import android.content.SharedPreferences

actual open class Settings private actual constructor(
    val name: String
) {
    private lateinit var settings: SharedPreferences

    constructor(
        name: String,
        context: Context
    ) : this(name) {
        this.settings = context.getSharedPreferences(name, Context.MODE_PRIVATE)
    }

    open val sharedPreferences: SharedPreferences?
        get() {
            return if (::settings.isInitialized) settings
            else null
        }

    actual fun contains(key: String): Boolean {
        return this.settings.contains(key)
    }

    actual fun <T> set(key: String, value: T) {
        with(settings.edit()) {
            when (value) {
                is String -> putString(key, value)
                is Int -> putInt(key, value)
                is Boolean -> putBoolean(key, value)
            }
            apply()
        }
    }

    actual fun getString(key: String, defaultValue: String): String {
        return this.settings.getString(key, defaultValue)
    }

    actual fun getInt(key: String, defaultValue: Int): Int {
        return this.settings.getInt(key, defaultValue)
    }

    actual fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return this.settings.getBoolean(key, defaultValue)
    }
}
