package nz.salect.kotlinidiom.store

import android.content.Context
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

actual class File actual constructor(
    var fileName: String
) {
    private lateinit var context: Context
    lateinit var file: File

    constructor(
        fileName: String,
        context: Context,
        rootDir: File = context.filesDir
    ) : this(fileName) {
        this.context = context
        this.file = File(rootDir, fileName)
    }

    actual fun changeFileName(newName: String) {
        if (fileName != newName) {
            fileName = newName
            this.file = File(context.filesDir, fileName)
        }
    }

    actual fun create(): Boolean {
        if (this.isFileExist()) return false
        return file.createNewFile()
    }

    actual fun save(content: String): Boolean {
        if (!this.isFileExist()) return false

        return try {
            FileOutputStream(file).use {
                it.write(content.toByteArray())
            }
            true
        } catch (e: Exception) {
            false
        }

    }

    actual fun delete(): Boolean {
        return file.delete()
    }

    actual fun isFileExist(): Boolean {
        return file.isFile && file.exists()
    }

    actual fun read(): String? {
        if (!this.isFileExist()) return null

        FileInputStream(file).bufferedReader().use {
            return it.readText()
        }
    }
}