package nz.salect.kotlinidiom.utils

import android.content.Context
import android.provider.Settings.Secure

actual open class DeviceId (
    private val context:Context
){
    actual open fun get():String {
        return Secure.getString(
            context.getContentResolver(),
            Secure.ANDROID_ID
        )
    }
}