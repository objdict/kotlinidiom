package nz.salect.kotlinidiom.http

import com.github.kittinunf.fuel.core.FileDataPart
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.httpUpload
import nz.salect.kotlinidiom.store.File
import nz.salect.objJSON.instanceFromJson

actual fun get(
    url: String,
    params: Map<String, String>?,
    headers: Map<String, String>?
): HttpResponse {
    FuelManager.instance.baseHeaders = headers

    val urlWithParams = combineUrlWithParams(url, params)
    val request = urlWithParams.httpGet()

    return handleResponse(request)
}

actual fun post(
    url: String,
    json: Map<String, Any>?,
    headers: Map<String, String>?
): HttpResponse {
    FuelManager.instance.baseHeaders = headers

    val request = url.httpPost().apply {
        body(mapToStr(json))
        json?.let { this.headers["Content-Type"] = "application/json" }
    }
    return handleResponse(request)
}

actual fun upload(
    url: String,
    json: Map<String, Any>?,
    files: Map<String, File>,
    headers: Map<String, String>?
): HttpResponse {
    FuelManager.instance.baseHeaders = headers

    val parameters = json?.pairsList
    val request = when (parameters) {
        null -> url.httpUpload()
        else -> url.httpUpload(parameters = parameters)
    }.apply {
        files.dataParts.forEach { add(it) }
    }
    return handleResponse(request)
}

internal fun handleResponse(
    request: Request
): HttpResponse {
    val (_, response, _) = request.responseString()

    val responseStatus = response.statusCode
    val responseBody = response.data.toString(Charsets.UTF_8)

    // TODO: check if objJson will give us the raw string when it can't parse
    val jsonMap = when {
        isJsonString(responseBody) -> responseBody.instanceFromJson()
        else -> null
    }
    return HttpResponse(
        statusCode = responseStatus,
        jsonResponse = jsonMap as Map<*, *>?,
        rawResponse = if (responseBody.isNotEmpty()) responseBody else null,
        responseMessage = response.responseMessage,
        request = request
    )
}

internal val Map<String, Any>.pairsList: List<Pair<String, Any>>
    get() = this.map { Pair(it.key, it.value) }

internal val Map<String, File>.dataParts: List<FileDataPart>
    get() = this.map { FileDataPart(it.value.file, it.key) }