package nz.salect.kotlinidiom.socket

import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.InetSocketAddress
import java.net.Socket

const val SPACE = ' '

actual class Socket actual constructor(
    actual val host: String,
    actual val port: Int,
    actual val timeOut: Int
) {
    private var socket: Socket = Socket()
    private val messageDelimiter = "\n"

    private val reader: InputStreamReader by lazy {
        InputStreamReader(socket.getInputStream())
    }

    private val writer: OutputStreamWriter by lazy {
        OutputStreamWriter(socket.getOutputStream())
    }

    init {
        connect()
    }

    actual fun connect() {
        val address = InetSocketAddress(host, port)
        try {
            socket.connect(address, timeOut)
        } catch (e: IOException) {
            println("kotlinidiom.Socket.connect: error connecting to $host on port $port")
            println(e)
        }
    }

    actual fun send(message: String) {
        writer.write(message)

        if (!message.endsWith(messageDelimiter)) {
            writer.write(messageDelimiter)
        }
        writer.flush()
    }

    actual fun read(): String? {
        var message = ""
        while (true) {
            val c = reader.read().toChar()
            if (c < SPACE) break
            message += c
        }
        return if (message.isEmpty()) null
        else message
    }

    actual fun close() {
        socket.close()
    }
}