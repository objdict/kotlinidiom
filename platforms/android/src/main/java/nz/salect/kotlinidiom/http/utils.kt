package nz.salect.kotlinidiom.http

import nz.salect.objJSON.Dumper
import java.net.URLEncoder

internal fun combineUrlWithParams(
    url: String,
    params: Map<String, String>?
): String {
    return if (params != null) {
        "$url?${mapToQueryData(params)}"
    } else url
}

internal fun mapToQueryData(params: Map<String, String>): String {
    val result = StringBuilder()
    for ((key, value) in params) {
        val encodedKey = URLEncoder.encode(key, "UTF-8")
        val encodedValue = URLEncoder.encode(value, "UTF-8")
        result.append("$encodedKey=$encodedValue&")
    }
    val lastIndex = result.length - 1
    result.deleteCharAt(lastIndex)
    return result.toString()
}

internal fun mapToStr(body: Map<String, *>?): String {
    return if (body == null) {
        ""
    } else {
        Dumper(body).dumps()
    }
}

internal fun isNotEmptyMap(obj: Any): Boolean {
    return obj is Map<*, *> && obj.keys.isNotEmpty()
}

internal fun isJsonString(targetString: String): Boolean {
    return targetString.startsWith("{") || targetString.startsWith("[")
}