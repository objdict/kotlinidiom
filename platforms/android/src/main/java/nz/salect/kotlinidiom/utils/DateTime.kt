package nz.salect.kotlinidiom.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.TimeZone

actual class DateTime {

    actual companion object {
        actual val dateFormat = "dd/MM/yyyy HH:mm:ss:SSS"

        actual val utcDate: String
            get() = utcDateTime.splitDateTime[0]

        actual val utcTime: String
            get() = utcDateTime.splitDateTime[1]

        actual val utcDateTime: String
        get(){
            val simpleDateFormat = SimpleDateFormat(dateFormat)

            simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
            return simpleDateFormat.format(Date())
        }

        actual fun getLocalTimeFromUTCString(
            utcDateString: String,
            utcTimeString: String
        ): String {
            val regDate = Regex("[0-3][0-9]/[0-1][0-9]/[0-9]{4}")
            val regTime = Regex("[0-3][0-9]:[0-5][0-9]:[0-5][0-9]:[0-9]{3}")

            return if (
                utcDateString.matches(regDate) &&
                utcTimeString.matches(regTime)
            ) {
                getLocalTimeFromUTCString("$utcDateString $utcTimeString")
            } else {
                " "
            }
        }

        actual fun getLocalTimeFromUTCString(utcDateTimeString: String): String {
            val simpleDateFormat = SimpleDateFormat(dateFormat)

            val date: Date? = try {
                simpleDateFormat.parse(utcDateTimeString)
            } catch (e: ParseException) {
                null
            }

            return if (date == null) {
                utcDateTimeString
            } else {
                simpleDateFormat.format(
                    addTimeZoneOffsetToTime(date)
                )
            }

        }

        private fun addTimeZoneOffsetToTime(date: Date): Date {
            val timeZoneOffset = Calendar.getInstance().run {
                timeZone.getOffset(time.time) / 3600000
            }
            val calendar = Calendar.getInstance()
            calendar.time = date
            calendar.add(Calendar.HOUR, timeZoneOffset)
            return calendar.time
        }
    }
}