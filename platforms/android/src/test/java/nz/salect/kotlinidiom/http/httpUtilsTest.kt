package nz.salect.kotlinidiom.http

import kotlin.test.*


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
class MapToQueryDataTest {
    @Test
    fun normalCase() {
        val params = mapOf(
            "name" to "albert",
            "age" to "35"
        )
        val queryString = mapToQueryData(params)
        assertEquals("name=albert&age=35", queryString)
    }

    @Test
    fun encode_space() {
        val params = mapOf(
            "name" to "albert gao",
            "age" to "35"
        )
        val queryString = mapToQueryData(params)
        assertEquals("name=albert+gao&age=35", queryString)
    }

    @Test
    fun encode_symbol() {
        val params = mapOf(
            "name" to "[albert]",
            "age" to "35"
        )
        val queryString = mapToQueryData(params)
        assertEquals("name=%5Balbert%5D&age=35", queryString)
    }
}
