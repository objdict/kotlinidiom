package nz.salect.kotlinidiom.socket

import kotlinx.coroutines.*
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.ServerSocketChannel
import java.nio.channels.SocketChannel
import java.nio.charset.StandardCharsets

class MockSocketServer(private val port: Int) : CoroutineScope {
    override val coroutineContext = Dispatchers.IO

    val receivedMessages = mutableListOf<String>()

    private lateinit var serverChannel: ServerSocketChannel

    init {
        serveSockets()
    }

    private fun serveSockets() = launch {
        try {
            serverChannel =
                ServerSocketChannel
                    .open()
                    .bind(InetSocketAddress(port))
        } catch (e: Throwable) {
            println("MockSocketServer.serveSockets: Error connecting to port $port\n$e")
        }

        while (isActive) {
            val client = serverChannel.accept()
            val address = try {
                val ia = client.remoteAddress as InetSocketAddress
                "${ia.address.hostAddress}:${ia.port}"
            } catch (ex: Throwable) {
                println("Accepted client connection but failed to get its address because of $ex")
                continue /* accept next connection */
            }
            println("Accepted client connection from $address")

            launch(coroutineContext) {
                try {
                    handleClient(client)
                    println("Client connection from $address has terminated normally")
                } catch (ex: Throwable) {
                    println("Client connection from $address has terminated because of $ex")
                }
            }
        }
    }

    private suspend fun handleClient(client: SocketChannel) {
        while (isActive) {
            val data = client.readString()

            data ?: break // socket closed

            receivedMessages.add(data.toString())

            println("MockSocketServer.handleClient: returning response --> $data")
            client.writeString("$data")
        }
    }

    fun close() {
        serverChannel.close()
    }
}

fun ByteBuffer.decodeToUtf8(): String = StandardCharsets.UTF_8.decode(this).toString()

fun String.toByteBuffer(): ByteBuffer = ByteBuffer.wrap(this.toByteArray())

suspend fun SocketChannel.writeString(string: String) = coroutineScope {
    launch(Dispatchers.IO) {
        val buffer: ByteBuffer = string.toByteBuffer()
        this@writeString.write(buffer)
    }
}

suspend fun SocketChannel.readString(capacity: Int = 1024): CharSequence? = coroutineScope {
    val buffer = ByteBuffer.allocate(capacity)

    val count = withContext(Dispatchers.IO) {
        this@readString.read(buffer)
    }
    if (count < 0) return@coroutineScope null

    buffer.flip()
    return@coroutineScope buffer.decodeToUtf8()
}