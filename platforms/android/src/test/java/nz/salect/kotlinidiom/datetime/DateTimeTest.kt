package nz.salect.kotlinidiom.datetime

import nz.salect.kotlinidiom.utils.DateTime
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

class DateTimeTest {

    private val dateRegex = Regex("[0-3][0-9]/[0-1][0-9]/[0-9]{4}")
    private val timeRegex = Regex("[0-3][0-9]:[0-5][0-9]:[0-5][0-9]:[0-9]{3}")

    @Test
    fun dateFormat() {
        val format = "dd/MM/yyyy HH:mm:ss:SSS"
        assertEquals(format, DateTime.dateFormat)
    }

    @Test
    fun utcDateAndTime() {
        val utcDateTime = DateTime.utcDateTime.split(" ")

        assertTrue(utcDateTime[0].matches(dateRegex))
        assertTrue(utcDateTime[1].matches(timeRegex))
    }

    @Test
    fun getLocalTimeFromUTCString() {
        val utcDateTime = DateTime.utcDateTime.split(" ")

        val localDateTime = DateTime.
            getLocalTimeFromUTCString(utcDateTime[0], utcDateTime[1])
            .split(" ")

        assertTrue(localDateTime[0].matches(dateRegex))
        assertTrue(localDateTime[1].matches(timeRegex))
    }

    @Test
    fun utcTimeIsActuallyDifferentToLocalTime() {
        /** This test will fail on a machine using UTC time...
         * worth checking it though, and I bet that won't happen.*/
        val utcDateTime = DateTime.utcDateTime
        val localDateTime = DateTime.getLocalTimeFromUTCString(utcDateTime)
        assertNotEquals(utcDateTime, localDateTime)
    }

    @Test
    fun getUtcDate() {
        assertEquals(DateTime.utcDateTime.split(" ")[0], DateTime.utcDate)
    }

    @Test
    fun getUtcTime() {
        assertEquals(DateTime.utcDateTime.split(" ")[1], DateTime.utcTime)
    }
}