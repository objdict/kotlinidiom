package nz.salect.kotlinidiom.socket

import io.mockk.every
import io.mockk.mockkConstructor
import io.mockk.unmockkConstructor
import org.junit.Test
import java.io.IOException
import java.net.InetAddress

private val host = InetAddress.getLoopbackAddress().hostAddress
private const val port = 8964

/**
 * If these tests fail for no apparent reason try changing the
 * port number, it may be in use on your machine.
 */
/* SocketTest must be removed in order to migrate to kotlin 1.3.0 because
   the MockSocketServer implementation is no longer supported by coroutines
   and needs to be fixed, but that will be difficult, see KHA-878

class SocketTest {
    companion object {
        private val server = MockSocketServer(port)

        @AfterClass
        @JvmStatic
        fun closeServer() {
            server.close()
        }
    }

    private val message = "Some message that does not contain a control character"
    private val twoMessages = "$message\n$message"

    private lateinit var socket: Socket

    @Before
    fun createSocket() {
        socket = Socket(host, port)
    }

    @After
    fun closeSocket() {
        socket.close()
    }

    private fun sendMessageAndWait(message: String): Socket {
        socket.send(message)
        //Just need a small delay for the message to arrive
        //10 seems to work. Increase if needed, as little as possible is best
        Thread.sleep(10)
        return socket
    }

    @Test
    fun readMessage() {
        val socket = sendMessageAndWait(message)
        val response = socket.read()
        assertEquals(message, response)
    }

    @Test
    fun readMultipleMessages() {
        val socket = sendMessageAndWait(twoMessages)
        val response1 = socket.read()
        assertEquals(message, response1)

        val response2 = socket.read()
        assertEquals(message, response2)
    }

    @Test
    fun sendMessage() {
        sendMessageAndWait(message)
        assertEquals("$message\n", server.receivedMessages[0])
    }
}*/

class SocketTestNoServer {
    @Test
    fun connectHandlesIOException() {
        mockkConstructor(java.net.Socket::class)
        every { anyConstructed<java.net.Socket>().connect(any()) } throws IOException()
        Socket(host, port)
        println("Test passed if line printed above is java.net.IOException")
        unmockkConstructor(java.net.Socket::class)
    }
}