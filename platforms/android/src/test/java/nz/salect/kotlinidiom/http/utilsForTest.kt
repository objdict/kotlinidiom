package nz.salect.kotlinidiom.http

import com.github.kittinunf.fuel.core.Request
import org.mockserver.client.server.MockServerClient
import org.mockserver.integration.ClientAndServer
import org.mockserver.model.Header.header
import org.mockserver.model.HttpRequest.request
import org.mockserver.model.HttpResponse.response
import org.mockserver.model.Parameter.param
import java.util.Random
import kotlin.test.AfterTest
import kotlin.test.BeforeTest

internal fun MockServerClient.setup(
    requestMethod: String,
    requestPath: String,
    responseStatus: Int,
    responseBody: String
) {

    this.`when`(
        request()
            .withMethod(requestMethod)
            .withPath(requestPath)

    )
        .respond(
            response()
                .withStatusCode(responseStatus)
                .withBody(responseBody)
        )
}

internal fun MockServerClient.verifyRequest(
    method: String?,
    path: String?,
    headers: Map<String, *>?,
    queryStrings: Map<String, *>? = null,
    bodyStr: String? = null
) {
    val request = request()

    method?.let { request.withMethod(method) }

    path?.let { request.withPath(path) }

    headers?.let {
        for ((key, value) in headers) {
            request.withHeader(
                header(key, value.toString())
            )
        }
    }

    queryStrings?.let {
        for ((key, value) in queryStrings) {
            request.withQueryStringParameters(
                param(key, value.toString())
            )
        }
    }

    bodyStr?.let { request.withBody(bodyStr) }

    this.verify(request)
}

val random = Random()
internal fun randomFrom(
    from: Int = 1024,
    to: Int = 65535
): Int {
    return random.nextInt(to - from) + from
}

abstract class HttpTestBase {
    protected val ok = "OK"

    protected val requestBody = mapOf("body" to "content")
    protected val responseBody = "{\"name\":\"albert\"}"

    protected val responseBodyError = "i am error"
    protected val responseBodyNotJson = "i am not json"

    protected val jsonResponse = mapOf("name" to "albert")

    protected val headers = mapOf("NAME-HEADER" to "test")
    protected val queryStrings = mapOf("age" to "16", "nz" to "yes")
    protected val bodyStr = "{\"body\": \"content\"}"

    protected val host = "localhost"
    protected var port = randomFrom()

    val url: String
        get() = "http://$host:$port"

    var mockServer: MockServerClient = MockServerClient(host, port)

    @BeforeTest
    fun prepare() {
        mockServer = ClientAndServer.startClientAndServer(port)
    }

    @AfterTest
    fun tearDown() {
        mockServer.close()
    }

    protected fun setMockResponse(statusCode: Int, body: String) {
        mockServer.setup(
            "POST",
            "/users",
            statusCode,
            body
        )
    }

    protected fun verifyPostRequest(headers: Map<String, *>, bodyStr: String) {
        mockServer.verifyRequest(
            method = "POST",
            path = "/users",
            headers = headers,
            queryStrings = null,
            bodyStr = bodyStr
        )
    }
}

internal val HttpResponse.fuelRequest: Request
    get() = this.request as Request