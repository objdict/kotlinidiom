package nz.salect.kotlinidiom

import android.content.Context
import android.content.SharedPreferences
import android.support.test.InstrumentationRegistry
import android.support.test.filters.SmallTest
import android.support.test.runner.AndroidJUnit4
import nz.salect.kotlinidiom.store.Settings
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
@RunWith(AndroidJUnit4::class)
@SmallTest
class SettingsInstrumentedTest {
    private val appContext = InstrumentationRegistry.getTargetContext()
    private val settingsName = "SettingForInstrumentedTest"
    private val notThere = "i-am-not-there"
    private val settings = Settings(settingsName, appContext)

    @After
    fun tearDown() {
        // SharedPreferences will be persisted unless you remove it
        val settings = appContext.getSharedPreferences(
            settingsName,
            Context.MODE_PRIVATE
        )
        val editor = settings.edit()
        editor.remove(notThere)
        editor.apply()
    }

    @Test
    @Throws(Exception::class)
    fun containsFalse() {
        val result = settings.contains(notThere)
        assertEquals(false, result)
    }

    @Test
    @Throws(Exception::class)
    fun containsTrue() {
        // Make sure the key is not there
        assertEquals(false, settings.contains(notThere))

        settings.set(notThere, "yes")

        val result = settings.contains(notThere)
        assertEquals(true, result)
    }

    @Test
    @Throws(Exception::class)
    fun setString_getString() {
        val expectedValue = "pretty"

        settings.set(notThere, expectedValue)

        val result = settings.getString(notThere)
        assertEquals(expectedValue, result)
    }

    @Test
    @Throws(Exception::class)
    fun setInt_getInt() {
        val expectedValue = 831023

        settings.set(notThere, expectedValue)

        val result = settings.getInt(notThere)
        assertEquals(expectedValue, result)
    }

    @Test
    @Throws(Exception::class)
    fun setBoolean_getBoolean() {
        val expectedValue = true

        settings.set(notThere, expectedValue)

        val result = settings.getBoolean(notThere)
        assertEquals(expectedValue, result)
    }

    @Test
    fun getSharedPreferences() {
        val sharedPreferences = settings.sharedPreferences
        assertTrue(sharedPreferences is SharedPreferences)
    }

    @Test
    fun getBooleanDefaultValue() {
        assertEquals(false, settings.getBoolean(notThere))
    }

    @Test
    fun getStringDefaultValue() {
        assertEquals("", settings.getString(notThere))
    }

    @Test
    fun getIntDefaultValue() {
        assertEquals(0, settings.getInt(notThere))
    }
}

