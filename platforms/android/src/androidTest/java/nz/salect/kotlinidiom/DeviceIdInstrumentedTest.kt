package nz.salect.kotlinidiom

import android.support.test.InstrumentationRegistry
import android.support.test.filters.SmallTest
import android.support.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

import nz.salect.kotlinidiom.utils.DeviceId

import org.junit.Assert.*

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
@RunWith(AndroidJUnit4::class)
@SmallTest
class DeviceIdInstrumentedTest {
    private val appContext = InstrumentationRegistry.getTargetContext()

    @Test
    @Throws(Exception::class)
    fun get_withContext() {
        // create the file from the lib
        val devId = DeviceId(appContext).get()
        assertNotEquals("", devId)
    }
}

