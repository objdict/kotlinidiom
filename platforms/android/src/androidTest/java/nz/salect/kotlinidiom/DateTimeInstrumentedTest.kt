package nz.salect.kotlinidiom

import android.support.test.InstrumentationRegistry
import android.support.test.filters.SmallTest
import android.support.test.runner.AndroidJUnit4
import nz.salect.kotlinidiom.utils.DateTime
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test
import org.junit.runner.RunWith
import java.text.SimpleDateFormat
import java.util.Calendar

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
@RunWith(AndroidJUnit4::class)
@SmallTest
class DateTimeInstrumentedTest {
    private val appContext = InstrumentationRegistry.getTargetContext()
    private val timeZoneOffset = Calendar.getInstance().run {
        timeZone.getOffset(time.time) / 3600000
    }
    private val hoursInADay = 24

    @Test
    @Throws(Exception::class)
    fun getUTCTimeString_time_should_be_different_than_local_time() {
        val dateFormat = SimpleDateFormat("HH:mm")
        val expectTime = dateFormat.format(Calendar.getInstance().time)

        val dateTimeStr = DateTime.utcDateTime
        val temp = (dateTimeStr.split(" "))[1]
        val result = temp.removeRange(temp.length - 3 until temp.length - 1)

        assertNotEquals(expectTime, result)
    }

    @Test
    @Throws(Exception::class)
    fun getUTCTimeString_the_offset_should_be_equal_to_timezone() {
        val dateFormat = SimpleDateFormat("HH:mm")

        val expectTime = dateFormat.format(Calendar.getInstance().time)
        val hourInExpectTime = expectTime.split(":")[0].toInt()
        var expectHour = if (hourInExpectTime + timeZoneOffset < hoursInADay) {
            hourInExpectTime + (hoursInADay - timeZoneOffset)
        } else hourInExpectTime - timeZoneOffset
        expectHour %= 24

        val dateTimeStr = DateTime.utcDateTime
        val temp = (dateTimeStr.split(" "))[1]
        val result = temp.removeRange(temp.length - 3 until temp.length - 1)
        val actualHour = result.split(":")[0].toInt()

        assertEquals(expectHour, actualHour)
    }

    @Test
    @Throws(Exception::class)
    fun getLocalTimeFromUTCString() {
        val source = DateTime.utcDateTime
        val expectHour = getHourFromDateString(source, timeZoneOffset)

        val dateTimeStr = DateTime.getLocalTimeFromUTCString(source)
        val actualHour = getHourFromDateString(dateTimeStr)

        assertEquals(expectHour, actualHour)
    }

    @Test
    @Throws(Exception::class)
    fun getLocalTimeFromUTCString_error_case() {
        val dateTimeStr = DateTime.getLocalTimeFromUTCString("", "a")

        assertEquals(" ", dateTimeStr)
    }

    private fun getHourFromDateString(dateStr: String, timeZoneOffset: Int = 0): Int {
        val time = dateStr.split(" ").last()
        val result = time.slice(0..1).toInt()
        return (result + timeZoneOffset) % hoursInADay
    }
}
