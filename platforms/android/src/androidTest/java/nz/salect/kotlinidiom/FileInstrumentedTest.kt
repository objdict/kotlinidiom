package nz.salect.kotlinidiom

import android.support.test.InstrumentationRegistry
import android.support.test.filters.SmallTest
import android.support.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

import nz.salect.kotlinidiom.store.*

import org.junit.Assert.*
import org.junit.Before
import java.io.File
import java.io.FileInputStream

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
@RunWith(AndroidJUnit4::class)
@SmallTest
class FileInstrumentedTest {
    private val appContext = InstrumentationRegistry.getTargetContext()
    private val fileName = "SettingForInstrumentedTest"
    private val androidFile = File(
        appContext.filesDir,
        fileName
    )
    private val content = "i-am-file-content"

    @Before
    fun prepare() {
        if (androidFile.exists()) androidFile.delete()
    }

    @Test
    @Throws(Exception::class)
    fun create() {
        // create the file from the lib
        val myFile = File(fileName, appContext)
        val result = myFile.create()

        assertEquals(androidFile, myFile.file)
        assertEquals(true, result)

        // assert the file existence via system API
        assertEquals(true, androidFile.exists())
        assertEquals(true, androidFile.isFile)
    }

    @Test
    @Throws(Exception::class)
    fun delete() {
        // create the file from the lib
        val myFile = File(fileName, appContext)
        myFile.create()

        myFile.delete()
        assertEquals(false, androidFile.exists())
        assertEquals(false, androidFile.isFile)
    }

    @Test
    @Throws(Exception::class)
    fun isFileExist() {
        // create the file from the lib
        val myFile = File(fileName, appContext)
        assertEquals(false, androidFile.exists())

        myFile.create()
        assertEquals(true, androidFile.exists())
    }

    @Test
    @Throws(Exception::class)
    fun save_noFileCase() {
        val myFile = File(fileName, appContext)
        assertEquals(false, myFile.save("abc"))
    }

    @Test
    @Throws(Exception::class)
    fun save() {
        val content = "abc"
        val myFile = File(fileName, appContext)
        myFile.create()

        assertEquals(true, myFile.save(content))

        FileInputStream(androidFile).bufferedReader().use {
            val fileContentViaAndroidAPI = it.readText()
            assertEquals(content, fileContentViaAndroidAPI)
        }
    }

    @Test
    @Throws(Exception::class)
    fun read_noFileCase() {
        val myFile = File(fileName, appContext)
        assertEquals(null, myFile.read())
    }

    @Test
    @Throws(Exception::class)
    fun read() {
        val content = "abc"
        val myFile = File(fileName, appContext)
        myFile.create()
        myFile.save(content)

        assertEquals(content, myFile.read())
    }
}

