package nz.salect.kotlinidiom.store


actual class Settings actual constructor(
    val name: String
) {

    actual fun contains(key: String): Boolean {
        return false
    }

    actual fun <T> set(key: String, value: T) {

    }

    actual fun get(key: String, defaultValue: String):String {
        return ""
    }

    actual fun get(key: String, defaultValue: Int):Int {
        return 0
    }

    actual fun get(key: String, defaultValue: Boolean):Boolean {
        return false
    }
}
