package nz.salect.kotlinidiom.store

actual class File actual constructor(
    var fileName:String
) {

    actual fun changeFileName(newName:String) {
        fileName = newName
    }

    actual fun create(): Boolean {
        return false
    }

    actual fun save(content:String): Boolean {
        return false
    }

    actual fun delete(): Boolean {
        return false
    }

    actual fun isFileExist(): Boolean {
        return false
    }

    actual fun read():String? {
        return ""
    }
}