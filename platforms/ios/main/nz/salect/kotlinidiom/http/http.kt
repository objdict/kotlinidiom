package nz.salect.kotlinidiom.http

import nz.salect.kotlinidiom.http.HttpResponse

actual fun get(
    url: String,
    params: Map<String, String>?,
    headers: Map<String, String>?
): HttpResponse {
    return HttpResponse(statusCode = 200)
}

actual fun post(
    url: String,
    json: Map<String, String>?,
    headers: Map<String, String>?
): HttpResponse {
    return HttpResponse(statusCode = 200)
}
