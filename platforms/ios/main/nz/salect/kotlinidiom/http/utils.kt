package nz.salect.kotlinidiom.http

import nz.salect.objJSON.Dumper
import nz.salect.objJSON.instanceFromJson

internal fun combineUrlWithParams(
    url: String,
    params: Map<String, String>?
): String {
    return if (params != null) {
//        "$url?${mapToQueryData(params)}"
        url
    } else url
}

internal fun mapToStr(body: Map<String, *>?):String {
    return if (body == null) {
        ""
    } else {
        Dumper(body).dumps()
    }
}

internal fun isNotEmptyMap(obj:Any): Boolean {
    return obj is Map<*,*> && obj.keys.isNotEmpty()
}

internal fun isJsonString(targetString:String): Boolean {
    return targetString.startsWith("{") || targetString.startsWith("[")
}

