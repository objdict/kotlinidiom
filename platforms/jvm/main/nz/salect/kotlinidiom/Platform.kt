package nz.salect.kotlinidiom

actual class Platform actual constructor(
  private val url: String
) {
  actual fun get():String {
    return "KN-JVM $url"
  }
}