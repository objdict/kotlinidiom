package nz.salect.kotlinidiom

import kotlin.test.*

class PlatformTest {
    @Test
    fun testGetPlatform() {
        val platform = Platform("ABC")
        assertEquals("KN-JVM ABC", platform.get())
    }
}
