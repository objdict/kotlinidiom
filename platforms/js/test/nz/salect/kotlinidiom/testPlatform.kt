package nz.salect.kotlinidiom

import kotlin.test.*

class HttpTest {
    @Test
    fun testGetPlatform() {
        val platform = Platform("ABC")
        assertEquals("Kotlin-JS ABC", platform.get())
    }
}
