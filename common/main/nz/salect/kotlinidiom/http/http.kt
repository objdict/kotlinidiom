package nz.salect.kotlinidiom.http

import nz.salect.kotlinidiom.store.File

/**
 * send a get http request
 * Remember to use it in an async way according to platform convention
 *
 * @param url the destination url
 * @param params sent in the query string
 * @param headers HTTP Headers to send
 *
 * @return The result of the http get call
 */
expect fun get(
    url: String,
    params: Map<String, String>? = null,
    headers: Map<String, String>? = null
): HttpResponse

/**
 * send a post request
 *
 * @param url the destination url
 * @param json json data to send in the body
 * @param headers HTTP Headers to send
 *
 * @return The result of the http get call
 */
expect fun post(
    url: String,
    json: Map<String, Any>?,
    headers: Map<String, String>? = null
): HttpResponse

/**
 * send an upload request
 *
 * @param url the destination url
 * @param files: list of file objects to upload
 * @param headers HTTP Headers to send
 *
 * @return The result of the http get call
 */
expect fun upload(
    url: String,
    json: Map<String, Any>?,
    files: Map<String, File>,
    headers: Map<String, String>? = null
): HttpResponse