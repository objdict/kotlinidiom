package nz.salect.kotlinidiom.http

/**
 * When no status code can be determined we get -1.
 * Case of either no internet on client side or no server is
 * found for the given URL.
 */
const val NO_STATUS_CODE = -1

/**
 * @param statusCode response HTTP status code
 * @param jsonResponse parsed [Map] of json data if response is json
 * @param rawResponse unprocessed [String] of response, set it parsing failed
 * @param data to add to the response during processing if desired
 * @param responseMessage typically a message associated with the status code i.e (404) Not Found
 * @param request the original request object that was used to get this response
 */
data class HttpResponse(
    val statusCode: Int,
    val jsonResponse: Map<*, *>? = null,
    val rawResponse: String? = null,
    var data: Any? = null,
    var responseMessage: String = "",
    val request: Any? = null
) {
    companion object {
        fun isError(statusCode: Int): Boolean = (statusCode / 100) != 2
    }

    val error: Boolean
        get() = isError(statusCode)
}