package nz.salect.kotlinidiom.store

/**
 * This is for storing and retrieving the settings for an application
 *
 * @param name The name of the settings
 */
expect open class Settings private constructor(
    name: String
) {
    fun contains(key: String): Boolean
    fun <T> set(key: String, value: T)

    fun getInt(key: String, defaultValue: Int = 0): Int
    fun getString(key: String, defaultValue: String = ""): String
    fun getBoolean(key: String, defaultValue: Boolean = false): Boolean
}
