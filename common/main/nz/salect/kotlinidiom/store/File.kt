package nz.salect.kotlinidiom.store

/**
 * Save file internally
 */
expect class File(
    fileName: String
) {
    fun create(): Boolean
    fun delete(): Boolean
    fun isFileExist(): Boolean

    /**
     * It will overwrite the file content with the content parameter.
     */
    fun save(content: String): Boolean

    fun read(): String?

    /**
     * Change the file name,
     * then the instance will point to the file with this new name
     */
    fun changeFileName(newName: String)
}