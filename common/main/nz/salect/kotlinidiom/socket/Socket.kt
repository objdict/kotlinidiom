package nz.salect.kotlinidiom.socket

expect class Socket(
    host: String,
    port: Int,
    timeOut: Int = 0
) {
    val host: String
    val port: Int
    val timeOut: Int

    fun connect()
    fun send(message: String)
    fun read(): String?
    fun close()
}