package nz.salect.kotlinidiom.utils

expect class DeviceId {
    fun get(): String
}
