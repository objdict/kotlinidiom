package nz.salect.kotlinidiom.utils

expect class DateTime {
    companion object {

        val dateFormat: String

        val utcDate: String
        val utcTime: String

        val utcDateTime: String

        fun getLocalTimeFromUTCString(
            utcDateString: String,
            utcTimeString: String
        ): String

        fun getLocalTimeFromUTCString(utcDateTimeString: String): String
    }
}

val String.splitDateTime: List<String>
    get() = this.split(" ")
